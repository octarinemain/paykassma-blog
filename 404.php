<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package blog_paykassa
 */

get_header();
?>
<span class="error-bg-figure-one"></span>
<span class="error-bg-figure-one-shadow"></span>

<span class="error-bg-figure-two"></span>
<span class="error-bg-figure-two-shadow"></span>

<span class="error-bg-figure-three"></span>

<span class="error-bg-figure-four"></span>



<div class="content container error-wrap">
	<div class="error">
		<svg viewBox="0 0 766 380" fill="none" xmlns="http://www.w3.org/2000/svg">
<g filter="url(#filter0_di_33:213)">
<path fill-rule="evenodd" clip-rule="evenodd" d="M396.035 300.966C407.614 300.966 417 291.579 417 280C417 268.421 407.614 259.035 396.035 259.035C384.456 259.035 375.069 268.421 375.069 280C375.069 291.579 384.456 300.966 396.035 300.966Z" fill="#B0B8FD"/>
</g>
<g opacity="0.67" filter="url(#filter1_di_33:213)">
<path fill-rule="evenodd" clip-rule="evenodd" d="M536.75 21.4844C542.411 21.4844 547 16.8953 547 11.2344C547 5.57346 542.411 0.984375 536.75 0.984375C531.089 0.984375 526.5 5.57346 526.5 11.2344C526.5 16.8953 531.089 21.4844 536.75 21.4844Z" fill="#909CFF"/>
</g>
<g filter="url(#filter2_di_33:213)">
<path fill-rule="evenodd" clip-rule="evenodd" d="M241.046 75.9379C249.029 75.9379 255.5 69.4668 255.5 61.4843C255.5 53.5018 249.029 47.0308 241.046 47.0308C233.064 47.0308 226.593 53.5018 226.593 61.4843C226.593 69.4668 233.064 75.9379 241.046 75.9379Z" fill="#909CFF"/>
</g>
<g filter="url(#filter3_di_33:213)">
<path fill-rule="evenodd" clip-rule="evenodd" d="M148.88 237.438C164.344 237.438 176.88 224.902 176.88 209.438C176.88 193.974 164.344 181.438 148.88 181.438C133.416 181.438 120.88 193.974 120.88 209.438C120.88 224.902 133.416 237.438 148.88 237.438Z" fill="white"/>
</g>
<g filter="url(#filter4_di_33:213)">
<path fill-rule="evenodd" clip-rule="evenodd" d="M627.75 203.5C637.553 203.5 645.5 195.553 645.5 185.75C645.5 175.947 637.553 168 627.75 168C617.947 168 610 175.947 610 185.75C610 195.553 617.947 203.5 627.75 203.5Z" fill="white"/>
</g>
<g filter="url(#filter5_d_33:213)">
<path d="M282.953 205.638H258.153V237.438H232.953V205.638H156.153V187.638L225.153 97.438H252.953L187.953 183.638H233.753V155.438H258.153V183.638H282.953V205.638ZM352.295 239.438C341.095 239.438 331.095 236.638 322.295 231.038C313.495 225.305 306.561 217.038 301.495 206.238C296.428 195.305 293.895 182.371 293.895 167.438C293.895 152.505 296.428 139.638 301.495 128.838C306.561 117.905 313.495 109.638 322.295 104.038C331.095 98.3046 341.095 95.438 352.295 95.438C363.495 95.438 373.495 98.3046 382.295 104.038C391.228 109.638 398.228 117.905 403.295 128.838C408.361 139.638 410.895 152.505 410.895 167.438C410.895 182.371 408.361 195.305 403.295 206.238C398.228 217.038 391.228 225.305 382.295 231.038C373.495 236.638 363.495 239.438 352.295 239.438ZM352.295 216.838C362.295 216.838 370.161 212.705 375.895 204.438C381.761 196.171 384.695 183.838 384.695 167.438C384.695 151.038 381.761 138.705 375.895 130.438C370.161 122.171 362.295 118.038 352.295 118.038C342.428 118.038 334.561 122.171 328.695 130.438C322.961 138.705 320.095 151.038 320.095 167.438C320.095 183.838 322.961 196.171 328.695 204.438C334.561 212.705 342.428 216.838 352.295 216.838ZM553.265 205.638H528.465V237.438H503.265V205.638H426.465V187.638L495.465 97.438H523.265L458.265 183.638H504.065V155.438H528.465V183.638H553.265V205.638Z" fill="url(#paint0_linear_33:213)"/>
</g>
<g filter="url(#filter6_di_33:213)">
<path fill-rule="evenodd" clip-rule="evenodd" d="M393.485 126.454C411.425 126.454 425.969 111.91 425.969 93.969C425.969 76.0282 411.425 61.4844 393.485 61.4844C375.544 61.4844 361 76.0282 361 93.969C361 111.91 375.544 126.454 393.485 126.454Z" fill="white"/>
</g>
<defs>
<filter id="filter0_di_33:213" x="255.069" y="249.035" width="281.931" height="291.931" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
<feFlood flood-opacity="0" result="BackgroundImageFix"/>
<feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" result="hardAlpha"/>
<feOffset dy="120"/>
<feGaussianBlur stdDeviation="60"/>
<feColorMatrix type="matrix" values="0 0 0 0 0.0235294 0 0 0 0 0.396078 0 0 0 0 0.627451 0 0 0 0.3 0"/>
<feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow_33:213"/>
<feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow_33:213" result="shape"/>
<feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" result="hardAlpha"/>
<feOffset dy="-10"/>
<feGaussianBlur stdDeviation="5"/>
<feComposite in2="hardAlpha" operator="arithmetic" k2="-1" k3="1"/>
<feColorMatrix type="matrix" values="0 0 0 0 0.564706 0 0 0 0 0.611765 0 0 0 0 1 0 0 0 1 0"/>
<feBlend mode="normal" in2="shape" result="effect2_innerShadow_33:213"/>
</filter>
<filter id="filter1_di_33:213" x="406.5" y="-9.01562" width="260.5" height="270.5" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
<feFlood flood-opacity="0" result="BackgroundImageFix"/>
<feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" result="hardAlpha"/>
<feOffset dy="120"/>
<feGaussianBlur stdDeviation="60"/>
<feColorMatrix type="matrix" values="0 0 0 0 0.0235294 0 0 0 0 0.396078 0 0 0 0 0.627451 0 0 0 0.3 0"/>
<feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow_33:213"/>
<feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow_33:213" result="shape"/>
<feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" result="hardAlpha"/>
<feOffset dy="-10"/>
<feGaussianBlur stdDeviation="5"/>
<feComposite in2="hardAlpha" operator="arithmetic" k2="-1" k3="1"/>
<feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0.207843 0 0 0 0 0.415686 0 0 0 0.3 0"/>
<feBlend mode="normal" in2="shape" result="effect2_innerShadow_33:213"/>
</filter>
<filter id="filter2_di_33:213" x="106.593" y="37.0308" width="268.907" height="278.907" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
<feFlood flood-opacity="0" result="BackgroundImageFix"/>
<feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" result="hardAlpha"/>
<feOffset dy="120"/>
<feGaussianBlur stdDeviation="60"/>
<feColorMatrix type="matrix" values="0 0 0 0 0.0235294 0 0 0 0 0.396078 0 0 0 0 0.627451 0 0 0 0.3 0"/>
<feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow_33:213"/>
<feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow_33:213" result="shape"/>
<feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" result="hardAlpha"/>
<feOffset dy="-10"/>
<feGaussianBlur stdDeviation="5"/>
<feComposite in2="hardAlpha" operator="arithmetic" k2="-1" k3="1"/>
<feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0.207843 0 0 0 0 0.415686 0 0 0 0.3 0"/>
<feBlend mode="normal" in2="shape" result="effect2_innerShadow_33:213"/>
</filter>
<filter id="filter3_di_33:213" x="0.879883" y="171.438" width="296" height="306" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
<feFlood flood-opacity="0" result="BackgroundImageFix"/>
<feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" result="hardAlpha"/>
<feOffset dy="120"/>
<feGaussianBlur stdDeviation="60"/>
<feColorMatrix type="matrix" values="0 0 0 0 0.0235294 0 0 0 0 0.396078 0 0 0 0 0.627451 0 0 0 0.3 0"/>
<feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow_33:213"/>
<feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow_33:213" result="shape"/>
<feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" result="hardAlpha"/>
<feOffset dy="-10"/>
<feGaussianBlur stdDeviation="7"/>
<feComposite in2="hardAlpha" operator="arithmetic" k2="-1" k3="1"/>
<feColorMatrix type="matrix" values="0 0 0 0 0.564706 0 0 0 0 0.611765 0 0 0 0 1 0 0 0 0.52 0"/>
<feBlend mode="normal" in2="shape" result="effect2_innerShadow_33:213"/>
</filter>
<filter id="filter4_di_33:213" x="490" y="158" width="275.5" height="285.5" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
<feFlood flood-opacity="0" result="BackgroundImageFix"/>
<feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" result="hardAlpha"/>
<feOffset dy="120"/>
<feGaussianBlur stdDeviation="60"/>
<feColorMatrix type="matrix" values="0 0 0 0 0.0235294 0 0 0 0 0.396078 0 0 0 0 0.627451 0 0 0 0.3 0"/>
<feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow_33:213"/>
<feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow_33:213" result="shape"/>
<feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" result="hardAlpha"/>
<feOffset dy="-10"/>
<feGaussianBlur stdDeviation="7"/>
<feComposite in2="hardAlpha" operator="arithmetic" k2="-1" k3="1"/>
<feColorMatrix type="matrix" values="0 0 0 0 0.564706 0 0 0 0 0.611765 0 0 0 0 1 0 0 0 0.52 0"/>
<feBlend mode="normal" in2="shape" result="effect2_innerShadow_33:213"/>
</filter>
<filter id="filter5_d_33:213" x="78.1523" y="26.438" width="597.112" height="344" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
<feFlood flood-opacity="0" result="BackgroundImageFix"/>
<feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" result="hardAlpha"/>
<feOffset dx="22" dy="31"/>
<feGaussianBlur stdDeviation="50"/>
<feColorMatrix type="matrix" values="0 0 0 0 0.537255 0 0 0 0 0.584314 0 0 0 0 0.960784 0 0 0 0.79 0"/>
<feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow_33:213"/>
<feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow_33:213" result="shape"/>
</filter>
<filter id="filter6_di_33:213" x="241" y="51.4844" width="304.969" height="314.969" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
<feFlood flood-opacity="0" result="BackgroundImageFix"/>
<feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" result="hardAlpha"/>
<feOffset dy="120"/>
<feGaussianBlur stdDeviation="60"/>
<feColorMatrix type="matrix" values="0 0 0 0 0.0235294 0 0 0 0 0.396078 0 0 0 0 0.627451 0 0 0 0.3 0"/>
<feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow_33:213"/>
<feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow_33:213" result="shape"/>
<feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" result="hardAlpha"/>
<feOffset dy="-10"/>
<feGaussianBlur stdDeviation="7"/>
<feComposite in2="hardAlpha" operator="arithmetic" k2="-1" k3="1"/>
<feColorMatrix type="matrix" values="0 0 0 0 0.564706 0 0 0 0 0.611765 0 0 0 0 1 0 0 0 0.52 0"/>
<feBlend mode="normal" in2="shape" result="effect2_innerShadow_33:213"/>
</filter>
<linearGradient id="paint0_linear_33:213" x1="187" y1="58.0001" x2="451" y2="354" gradientUnits="userSpaceOnUse">
<stop stop-color="#C6B6FA"/>
<stop offset="1" stop-color="#7E88D8"/>
</linearGradient>
</defs>
</svg>

		<p class="error__subtitile">Page not found</p>
		<p class="error__text">We couldn’t find the page you were looking for. </p>
		<a class="error__btn" href="<?php echo get_option('home'); ?>">Back to the main page</a>
	</div>
</div>