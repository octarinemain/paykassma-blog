<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package blog_paykassa
 */

?>

	<footer class="footer">
		<div class="container">
      <div class="footer-content">
				<div class="footer-content__menu-wrap">
					<div class="main-logo-wrap">
						<a href="https://paykassma.com/" class="main-logo">
							<svg>
									<use xlink:href="<?php echo get_template_directory_uri(); ?>/front/dist/images/svg/sprite.svg#icon-logo"></use>
							</svg>
						</a>
					</div>
					<div class="footer-content__menu">
							<?php
							if( has_nav_menu( 'footer-1' ) ){
							wp_nav_menu(
								array(
									'theme_location' => 'footer-1',
									'menu_id'        => 'footer-1',
								)
							);
							}
							?>
							<?php
							if( has_nav_menu( 'footer-2' ) ){
							wp_nav_menu(
								array(
									'theme_location' => 'footer-2',
									'menu_id'        => 'footer-2',
								)
							);
							}
							?>
							<?php
							if( has_nav_menu( 'footer-3' ) ){
							wp_nav_menu(
								array(
									'theme_location' => 'footer-3',
									'menu_id'        => 'footer-3',
								)
							);
							}
							?>
						<?php
						if( has_nav_menu( 'footer-4' ) ){
							wp_nav_menu(
								array(
									'theme_location' => 'footer-4',
									'menu_id'        => 'footer-4',
								)
							);
						}
							?>
						<?php
						if( has_nav_menu( 'footer-5' ) ){
							wp_nav_menu(
								array(
									'theme_location' => 'footer-5',
									'menu_id'        => 'footer-5',
								)
							);
						}
							?>
						<?php
						if( has_nav_menu( 'footer-6' ) ){
							wp_nav_menu(
								array(
									'theme_location' => 'footer-6',
									'menu_id'        => 'footer-6',
								)
							);
						}
							?>
						<?php
						if( has_nav_menu( 'footer-7' ) ){
							wp_nav_menu(
								array(
									'theme_location' => 'footer-7',
									'menu_id'        => 'footer-7',
								)
							);
						}
							?>
						<?php
						if( has_nav_menu( 'footer-8' ) ){
							wp_nav_menu(
								array(
									'theme_location' => 'footer-8',
									'menu_id'        => 'footer-8',
								)
							);
						}
							?>
						<?php
						if( has_nav_menu( 'footer-9' ) ){
							wp_nav_menu(
								array(
									'theme_location' => 'footer-9',
									'menu_id'        => 'footer-9',
								)
							);
						}
							?>
						</div><!-- .footer-content__menu -->
				</div><!-- .footer-content__menu-wrap -->
				<p class="footer-content__copyright">© 2021 Paykassma. All rights reserved</p>
			</div><!-- .footer-content -->
		</div><!-- .container -->
	</footer><!-- .footer -->

<?php wp_footer(); ?>
</div> <!-- .wrap-content -->
</body>
</html>
