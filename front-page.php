<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package blog_paykassa
 */

get_header();
?>
<div class="social-networks container">
	<?php if(get_field('instagram', 'option')){ ?>
		<noindex><a class="social-networks__item instagram" rel="nofollow" href="<?php the_field('instagram', 'option'); ?>">
			<svg viewBox="0 0 17 17" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M16.945 5.018a6.144 6.144 0 0 0-.395-2.089A4.344 4.344 0 0 0 14.064.448 6.246 6.246 0 0 0 12 .052C11.092.012 10.804 0 8.5 0 6.193 0 5.897 0 4.996.052a6.245 6.245 0 0 0-2.063.396A4.35 4.35 0 0 0 .448 2.929a6.168 6.168 0 0 0-.395 2.06C.013 5.895 0 6.182 0 8.484c0 2.301 0 2.595.053 3.496.014.705.147 1.4.395 2.062a4.344 4.344 0 0 0 2.487 2.48c.658.258 1.356.401 2.063.424.909.04 1.197.053 3.502.053 2.305 0 2.6 0 3.502-.053a6.228 6.228 0 0 0 2.064-.395 4.357 4.357 0 0 0 2.486-2.481c.248-.66.381-1.355.395-2.061.04-.907.053-1.194.053-3.497-.002-2.301-.002-2.593-.055-3.495Zm-8.452 7.82A4.36 4.36 0 0 1 4.13 8.48a4.36 4.36 0 0 1 4.364-4.357c1.158 0 2.268.46 3.086 1.276a4.353 4.353 0 0 1 0 6.162 4.368 4.368 0 0 1-3.086 1.276Zm4.538-7.86a1.018 1.018 0 0 1-1.017-1.016 1.014 1.014 0 0 1 1.017-1.015 1.019 1.019 0 0 1 1.017 1.015c0 .562-.455 1.016-1.017 1.016Z" fill="url(#aa)"/><path d="M8.5 11a2.5 2.5 0 1 0 0-5 2.5 2.5 0 0 0 0 5Z" fill="url(#bb)"/><defs><linearGradient id="aa" x1="14.571" y1="1.821" x2="0" y2="17" gradientUnits="userSpaceOnUse"><stop stop-color="#9E45AF"/><stop offset=".502" stop-color="#E4265B"/><stop offset="1" stop-color="#FFDB0F"/></linearGradient><linearGradient id="bb" x1="8.5" y1="6" x2="8.5" y2="11" gradientUnits="userSpaceOnUse"><stop stop-color="#BD3180"/><stop offset="1" stop-color="#EF6B3A"/></linearGradient></defs></svg>
		</a></noindex>
	<?php } ?>
	<?php if(get_field('telegram', 'option')){ ?>
	<noindex><a class="social-networks__item telegram" rel="nofollow" href="<?php the_field('telegram', 'option'); ?>">
		<svg>
			<use xlink:href="<?php echo get_template_directory_uri(); ?>/front/dist/images/svg/sprite.svg#icon-telegram"></use>
		</svg>
	</a></noindex>
	<?php } ?>
	<?php if(get_field('twitter', 'option')){ ?>
	<noindex><a class="social-networks__item twitter" rel="nofollow" href="<?php the_field('twitter', 'option'); ?>">
		<svg>
			<use xlink:href="<?php echo get_template_directory_uri(); ?>/front/dist/images/svg/sprite.svg#icon-twitter"></use>
		</svg>
	</a></noindex>
	<?php } ?>
	<?php if(get_field('facebook', 'option')){
	?>
	<noindex><a class="social-networks__item facebook" rel="nofollow" href="<?php the_field('facebook', 'option'); ?>">
		<svg>
			<use xlink:href="<?php echo get_template_directory_uri(); ?>/front/dist/images/svg/sprite.svg#icon-facebook"></use>
		</svg>
	</a></noindex>
	<?php } ?>
</div>
<div class="beardcrumbs container"><?php if( function_exists('kama_breadcrumbs') ) kama_breadcrumbs(' / '); ?></div>
<main class="content container">
	<h1 class="home__title">Paykassma Blog</h1>
	<span class="header-bg-figure"></span>
	<?php
	$banner_posts = get_field('posts_to_banner', 'options');
	if($banner_posts){
	?>

	<!-- <div id="main-slider" class="splide data-slider">
        <div class="splide__track">
            <ul class="splide__list">
				<!- <?php while (have_rows('object_imgs')): the_row();?>
					<li class="splide__slide default-slide ">
						<div class="slide-img-wrapper">
							<?php if(get_sub_field('item')){ ?>
								<img src="<?php echo wp_get_attachment_image_url(get_sub_field('item'), 'full'); ?>" srcset="<?php echo wp_get_attachment_image_url(get_sub_field('item'), 'medium'); ?> 300w, <?php echo wp_get_attachment_image_url(get_sub_field('item'), 'medium_large'); ?> 768w, <?php echo wp_get_attachment_image_url(get_sub_field('item'), 'large'); ?> 1000w" alt="">
							<?php } ?>
						</div>
					</li>
                <?php endwhile; ?> ->


				<?php
					$i = 1;
					foreach($banner_posts as $post):
					setup_postdata($post);
				?>
					<li class="splide__slide default-slide ">
						<img data-content-id="<?php echo $i ?>" src="<?php echo wp_get_attachment_image_url(get_post_thumbnail_id($post->ID), 'full'); ?>" srcset="<?php echo wp_get_attachment_image_url(get_post_thumbnail_id($post->ID), 'medium'); ?> 300w, <?php echo wp_get_attachment_image_url(get_post_thumbnail_id($post->ID), 'medium_large'); ?> 768w, <?php echo wp_get_attachment_image_url(get_post_thumbnail_id($post->ID), 'large'); ?> 1000w" alt="" alt="post-img">
					</li>
				<?php 
					$i++; 
					endforeach; 
					wp_reset_postdata(); 
				?>
            </ul>
        </div>
        <div class="splide__arrows">
            <button class="splide__arrow splide__arrow--prev default-prev">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 80 64.6">
                    <path
                        d="M4 36.3h62.3L44.9 57.7a4 4 0 000 5.7 4 4 0 005.7 0l28.3-28.3.5-.6.2-.3.2-.4.1-.4.1-.3.1-.8-.1-.8-.1-.3-.1-.4-.2-.4-.2-.3-.5-.6L50.5 1.2a4 4 0 00-5.7 0 4 4 0 000 5.7l21.5 21.5H4c-2.2 0-4 1.8-4 4s1.8 3.9 4 3.9z"
                        fill="#fff" />
                </svg>
            </button>
            <button class="splide__arrow splide__arrow--next default-next">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 80 64.6">
                    <path
                        d="M4 36.3h62.3L44.9 57.7a4 4 0 000 5.7 4 4 0 005.7 0l28.3-28.3.5-.6.2-.3.2-.4.1-.4.1-.3.1-.8-.1-.8-.1-.3-.1-.4-.2-.4-.2-.3-.5-.6L50.5 1.2a4 4 0 00-5.7 0 4 4 0 000 5.7l21.5 21.5H4c-2.2 0-4 1.8-4 4s1.8 3.9 4 3.9z"
                        fill="#fff" />
                </svg>
            </button>
        </div>
    </div> -->



	<div class="main-slider">
		<div class="main-slider__left">
			<!-- <div class="main-slider__left-inner"> -->
		<!-- <div class="header-content__slider-wrap">
			<div class="header-content__slider-inner">
				<div class="header-content__slider-item"> -->

			<?php
				$i = 0;
				foreach($banner_posts as $post):
				setup_postdata($post);
				if ($i == 0) { 
			?>
				<img data-content-id="0" class="active" src="<?php echo wp_get_attachment_image_url(get_post_thumbnail_id($post->ID), 'full'); ?>" srcset="<?php echo wp_get_attachment_image_url(get_post_thumbnail_id($post->ID), 'medium'); ?> 300w, <?php echo wp_get_attachment_image_url(get_post_thumbnail_id($post->ID), 'medium_large'); ?> 768w, <?php echo wp_get_attachment_image_url(get_post_thumbnail_id($post->ID), 'large'); ?> 1000w" alt="" alt="post-img">
			<?php  } else { ?> 
				<img data-content-id="<?php echo $i ?>" src="<?php echo wp_get_attachment_image_url(get_post_thumbnail_id($post->ID), 'full'); ?>" srcset="<?php echo wp_get_attachment_image_url(get_post_thumbnail_id($post->ID), 'medium'); ?> 300w, <?php echo wp_get_attachment_image_url(get_post_thumbnail_id($post->ID), 'medium_large'); ?> 768w, <?php echo wp_get_attachment_image_url(get_post_thumbnail_id($post->ID), 'large'); ?> 1000w" alt="" alt="post-img">
			<?php 
				}
				$i++; 
				endforeach; 
				wp_reset_postdata(); 
			?>
			<!-- </div> -->
		</div>
		<div class="main-slider__right">
			<!-- <div class="main-slider__right-inner"> -->
		<?php
				$i = 0;
				foreach($banner_posts as $post):
				setup_postdata($post);
				if ($i == 0) { 
			?>
			
				<a data-content-id="<?php echo $i ?>" class="active" href="<?php the_permalink(); ?>">
					<h4><?php the_title(); ?></h4>
					<div class="main-slider__right-text-margin"><p><?php echo strip_tags(get_the_excerpt()); ?></p></div>
					<span class="read-more-btn">Read more</span>
				</a>
			<?php } else { ?>
				<a data-content-id="<?php echo $i ?>" href="<?php the_permalink(); ?>">
					<h4><?php the_title(); ?></h4>
					<div class="main-slider__right-text-margin"><p><?php echo strip_tags(get_the_excerpt()); ?></p></div>
					<span class="read-more-btn">Read more</span>
				</a>
			<?php 
				}
				$i++; 
				endforeach; 
				wp_reset_postdata(); 
			?>
			<!-- </div> -->
			<div class="circle-button">
			<?php
				$i = 0;
				foreach($banner_posts as $post):
				if ($i == 0) { 
			?>
				<span data-btn-id="0" class="circle-button__item active" type="button"></span>
			<?php } else { ?>
				<span data-btn-id="<?php echo $i ?>" class="circle-button__item" type="button"></span>
			<?php 
				}
				$i++; 
				endforeach; 
			?>
			</div>
		</div>
		<?php  ?>
	</div>
	<?php } ?>

	<span class="home-bg-figure-slider"></span>

	<!-- <div class="cats">
		<ul class="main-filter__list filter-wrap">
            <li class="main-filter__item <?php if (!isset($_GET['type'])): echo 'active'; endif; ?>">
                <a href="/"><?php echo __('All', '4rabet'); ?></a>
            </li>
			<?php 
			$tags = get_tags([
				'orderby'      => 'id',
				'order'        => 'ASC',
				'hide_empty'   => true,
				'fields'       => 'all',
			]);
			if($tags){ foreach($tags as $tag):
			$term = get_term($tag);
			?>
			<li class="main-filter__item <?php if (isset($_GET['type']) && $_GET['type'] == $term->slug): echo 'active'; endif; ?>">
                <a href="?type=<?php echo $term->slug; ?>"><?php echo $term->name; ?></a>
            </li>
			<?php
			endforeach; }
			?>
		</ul>
	</div> -->
		<?php
		
		$paged = 1;
        if (get_query_var('paged')) $paged = get_query_var('paged');
        if (get_query_var('page')) $paged = get_query_var('page');
		if (!isset($_GET['type'])){
			$wp_query = new WP_Query(array(
				'posts_per_page' => 8,
				'paged' => $paged,
			));
		}else{
           $wp_query = new WP_Query(array(
			   'posts_per_page' => 8,
			   'tax_query' => array(
				array(
					'taxonomy' => 'post_tag',
					'field'    => 'slug',
					'terms'    => $_GET['type'],
					),
				),
			   'paged' => $paged,
		   ));
		}
		?>
		<?php if ($wp_query->have_posts()): ?>
			<div class="posts__item-wrap">
					<?php while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
							<a href="<?php the_permalink(); ?>" class="posts__item">
									<div class="posts__item-img">
											<img src="<?php echo wp_get_attachment_image_url(get_post_thumbnail_id(get_the_ID()), 'full'); ?>" srcset="<?php echo wp_get_attachment_image_url(get_post_thumbnail_id(get_the_ID()), 'medium'); ?> 300w, <?php echo wp_get_attachment_image_url(get_post_thumbnail_id(get_the_ID()), 'medium_large'); ?> 768w, <?php echo wp_get_attachment_image_url(get_post_thumbnail_id(get_the_ID()), 'large'); ?> 1000w" alt="" alt="post-img">
											<div class="posts__categories">
													<?php $cats = wp_get_post_terms(get_the_ID(), 'post_tag', array('fields' => 'names'));
															$jj=0;
															foreach ($cats as $cat) :
																if($jj<3) {
													?>
													<p>
															<span class="posts__categories-item">
																	<?php echo $cat; ?>
															</span>
													</p>
													<?php $jj++; } endforeach; ?>
											</div>
									</div>
									<p class="posts__item-title"><?php the_title(); ?></p>
									<p class="posts__item-subtitle"><?php echo get_the_excerpt(); ?></p>
									<p class="read-more-btn">Read more</p>
							</a>
					<?php endwhile; ?>
					<div class="posts__item posts__item--empty"></div>
					<div class="posts__item posts__item--empty"></div>
					<div class="posts__item posts__item--empty"></div>
					<div class="posts__item posts__item--empty"></div>
			</div>
		<?php endif;?>
		<?php the_posts_pagination(array(
            'show_all' => False,
            'end_size' => 1,
            'mid_size' => 1,
            'screen_reader_text' => __(' '),
            'prev_next' => true,
            'prev_text' => __(''),
            'next_text' => __(''),
        ));
        ?>
	<?php if($wp_query->found_posts > 9): ?>
	<div class="related-posts">
        <?php
        $wp_query = new WP_Query(array(
            'posts_per_page' => 4,
            'post_status' => 'publish',
            'orderby' => 'rand',
        ));
        ?>
        <?php if ($wp_query->have_posts()) : ?>
           <h2 class="related-posts__title">You may like</h2>
           <div class="posts__item-wrap">
                <?php while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
                    <a href="<?php the_permalink(); ?>" class="posts__item">
                        <div class="posts__item-img">
                            <img src="<?php echo wp_get_attachment_image_url(get_post_thumbnail_id(get_the_ID()), 'full'); ?>" srcset="<?php echo wp_get_attachment_image_url(get_post_thumbnail_id(get_the_ID()), 'medium'); ?> 300w, <?php echo wp_get_attachment_image_url(get_post_thumbnail_id(get_the_ID()), 'medium_large'); ?> 768w, <?php echo wp_get_attachment_image_url(get_post_thumbnail_id(get_the_ID()), 'large'); ?> 1000w" alt="" alt="post-img">
                            <div class="posts__categories">
                                <?php $cats = wp_get_post_terms(get_the_ID(), 'post_tag', array('fields' => 'names'));
								$j=0;
								foreach ($cats as $cat) :
									if($j<3) {
                                ?>
                                <p>
                                    <span class="posts__categories-item">
                                        <?php echo $cat; ?>
                                    </span>
                                </p>
                                <?php $j++; } endforeach; ?>
                            </div>
                        </div>
                        <p class="posts__item-title"><?php the_title(); ?></p>
                        <p class="posts__item-subtitle"><?php echo get_the_excerpt(); ?></p>
                        <p class="read-more-btn">Read more</p>
                    </a>
                <?php endwhile; ?>
                <div class="posts__item posts__item--empty"></div>
                <div class="posts__item posts__item--empty"></div>
                <div class="posts__item posts__item--empty"></div>
                <div class="posts__item posts__item--empty"></div>
            </div> 
        <?php endif; ?>
    </div>
	<?php endif; ?>
</main>

<?php
get_footer();