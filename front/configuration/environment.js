const path = require('path');
const portFinderSync = require('portfinder-sync');

const port = portFinderSync.getPort(8000);

module.exports = {
    paths: {
        source: path.resolve(__dirname, '../src/'),
        output: path.resolve(__dirname, '../dist/'),
        views: path.resolve(__dirname, '../../**/*.php'),
        images: path.resolve(__dirname, '../src/images/'),
        fonts: path.resolve(__dirname, '../src/fonts/'),
        assetsPath: process.env.ASSET_PATH || '/front/dist/',
    },
    server: {
        proxy: 'http://4rabet',
        host: '4rabet',
        port,
    },
};
