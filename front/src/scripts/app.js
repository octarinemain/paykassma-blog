import { trigerMenu } from '@components/burgerMenu';
import { positionLastElement, checkFoldingLastElement, foldingLastElement } from '@components/menu';
import { wrapping } from '@components/wrapScroll';
import { WIDTH_CELL_TABLE } from '@helpers/constants';
import { initSlider, autoNextSlide } from '@components/slider';
//import { initSliders } from '@components/swiper';
import { ratingBtn, ratingRender } from '@components/rating';
import { initRatingPost } from '@components/ratingPosts';

document.addEventListener("DOMContentLoaded", function(event) {
    // NOTE: menu
    {
        const $header = document.querySelector('.js-header');
        if ($header) {
            const $menu = $header.querySelector('.js-burger-menu');
            const $btn = $header.querySelector('.js-burger-btn');

            trigerMenu($menu, $btn, $header);

            //document.addEventListener("DOMContentLoaded", function(event) {
            setTimeout(() => checkFoldingLastElement($header), 200);
            //});

            const $arrayMenu = $header.querySelectorAll('#primary-menu > .menu-item-has-children > .sub-menu-wrap');
            positionLastElement($header, $arrayMenu);
        }
    }

    // SLIDER
    {
        const $slider = document.querySelector(".main-slider");

        if ($slider) {
            const $btns = $slider.querySelectorAll('[data-btn-id]');
            if ($btns.length) {
                const myTimer = setInterval(() => { autoNextSlide($slider, $btns); }, 6000);
                initSlider($slider, $btns, myTimer);
            }
        }
    }

    //DYNAMIC TABLE CELL
    {
        const $tables = document.querySelectorAll('table');
        if ($tables.length) {
            $tables.forEach($table => {
                const tdLength = $table.querySelectorAll('tbody > tr:first-child td').length;
                $table.style.minWidth = tdLength * WIDTH_CELL_TABLE + 'px';
            });
        }
    }

    //WRAPPING TABLE & IFRAME
    {
        const $tables = document.querySelectorAll('.visual-editor table'),
            $iframes = document.querySelectorAll('.visual-editor iframe');
        if ($tables.length) {
            $tables.forEach($table => {
                wrapping($table, 'table');
            });
        }
        if ($iframes.length) {
            $iframes.forEach($iframe => {
                wrapping($iframe, 'iframe');
            });
        }
    }


    // Set Rating
    {
        const $btnsRating = document.querySelectorAll('.rating__btn');

        if ($btnsRating.length) {
            ratingRender();
            ratingBtn();
        }
    }

    // Rating
    {
        const $ratings = document.querySelectorAll('[data-rating]');
        if ($ratings.length) {
            for (let i = 0; i < $ratings.length; i++) {
                initRatingPost($ratings[i]);
            }
        }
    }

    // NOTE: initSwiper
    {
        const $sliders = document.querySelectorAll('[data-slider]');

        if ($sliders.length) {
            for (let i = 0; i < $sliders.length; i++) {
                initSlider($sliders[i]);
            }
        }
    }
});
