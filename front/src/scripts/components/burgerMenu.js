export const trigerMenu = ($menu, $btn, $header) => {
    const openMenu = () => {
        $btn.classList.toggle('open-menu');
        $header.classList.toggle('open-menu');
        if ($btn.classList.contains('open-menu')) {
            $menu.style.maxHeight = $menu.scrollHeight + 'px';
        } else {
            $menu.style.maxHeight = 0;
        }
    };

    $btn.addEventListener('click', openMenu);
    addMenuEvent($menu, $header);
};

const addMenuEvent = ($menu, $header) => {
    $menu.addEventListener('click', event => {
        const target = event.target;

        if (target.classList.contains('menu-item-has-children')) {
            event.stopPropagation();
            const $parent = target.closest('.open-sub-menu');
            let $openItemMenu = [];

            if ($parent) {
                $openItemMenu = $header.querySelectorAll(`#${$parent.id} >.sub-menu .open-sub-menu`);
            } else {
                const $openItemMainMenu = $header.querySelector('#primary-burger-menu > .open-sub-menu');
                if ($openItemMainMenu) {
                    $openItemMainMenu.classList.remove('open-sub-menu');
                    $openItemMenu = $openItemMainMenu.querySelectorAll(`.sub-menu .open-sub-menu`);
                }
            }
            if ($openItemMenu.length) {
                $openItemMenu.forEach(item => item.classList.remove('open-sub-menu'));
            }

            if (target.classList.contains('open-sub-menu')) {
                target.classList.remove('open-sub-menu');
                $menu.style.maxHeight = $menu.scrollHeight + 'px';
            } else {
                target.classList.add('open-sub-menu');
                $menu.style.maxHeight = $menu.scrollHeight + target.querySelector('.sub-menu').scrollHeight + 46 + 'px';
            }
        }
    });
}
