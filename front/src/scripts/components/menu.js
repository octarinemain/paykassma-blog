export const positionLastElement = ($header, $arrayMenu) => {
    const headerEnd = $header.getBoundingClientRect().right;
    const lengthMenu = $arrayMenu.length;
    if (lengthMenu) {
        let i = 1;
        const paddingLeftWrap = 10;
        while (lengthMenu - i >= 0 && i < 3) {
            const indent = headerEnd - $arrayMenu[lengthMenu - i].getBoundingClientRect().right - paddingLeftWrap;
            if (indent < 0) {
                $arrayMenu[lengthMenu - i].style.left = indent + 'px';
            }
            i++;
        }
    }
}

export const checkFoldingLastElement = ($header) => {
    const contentWidth = $header.getBoundingClientRect().width - 178 - 120;
    if (contentWidth > 0) {
        const $menu = $header.querySelector('.menu-desktop-wrap');
        const $menuContent = $menu.querySelector('.menu');

        $menu.classList.add('show');
        foldingLastElement($header, $menu, $menuContent);
    }
}

export const foldingLastElement = ($header, $menu, $menuContent) => {
    const contentWidth = $header.getBoundingClientRect().width - 178 - 120;
    let menuWidth = $menu.getBoundingClientRect().width;
    if (contentWidth <= menuWidth) {
        let $other = $menuContent.querySelector('.menu-item-other');
        if (!$other) {
            $other = document.createElement('li');
            $other.classList.add('menu-item', 'menu-item-type-custom', 'menu-item-object-custom', 'menu-item-has-children', 'menu-item-other');
            $other.innerHTML = `<a href="#">Other</a><div class="sub-menu-wrap"><ul class="sub-menu"></ul></div>`;
            $menuContent.appendChild($other);
            menuWidth = $menu.getBoundingClientRect().width;
        }

        const $arrayMenu = $menu.querySelectorAll('.menu>li');
        const lengthArrayMenu = $arrayMenu.length;
        let i = 1;
        const $otherMenu = $other.querySelector('.sub-menu-wrap>.sub-menu');
        if (lengthArrayMenu) {
            while (contentWidth <= menuWidth) {
                i++;
                menuWidth = menuWidth - $arrayMenu[lengthArrayMenu - i].clientWidth - 60;
                $otherMenu.appendChild($arrayMenu[lengthArrayMenu - i]);
            }
        }
    }
}

window.addEventListener("resize", () => {

    const $header = document.querySelector('.js-header');
    const $menu = $header.querySelector('.menu-desktop-wrap');
    const $menuContent = $menu.querySelector('.menu');
    foldingLastElement($header, $menu, $menuContent);
});