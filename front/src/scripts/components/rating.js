import { getCookie, setCookie } from '@scripts/helpers/cookie';
import axios from 'axios';
import { generateCancelRequest } from '@helpers/axios';
import { warn } from '@helpers/utils';

if (getCookie('vote')) {
    document.querySelectorAll('.rating__empty').forEach(rating => {
        rating.classList.add('not-allowed');
    });
}

export const ratingRender = data => {
    const $ratingFill = document.querySelectorAll('.rating__blue');
    const $ratingValue = document.querySelector('.rating__value');
    const $votesValue = document.querySelectorAll('.votes__value');

    if (data) {
        $ratingFill.forEach(rating => {
            const pixel = calcPercent(data.rating, rating);
            rating.style.width = `${pixel}px`;
        });
        $ratingValue.innerText = parseFloat(data.rating).toFixed(2);
        $votesValue.forEach(value => {
            value.innerText = data.votes;
        });
    } else {
        const val = $ratingValue.innerText;
        $ratingFill.forEach(rating => {
            const pixel = calcPercent(val, rating);
            rating.style.width = `${pixel}px`;
        });
    }
};

const calcPercent = (value, child) => {
    return child.closest('.rating__empty').clientWidth * (value / 5);
};

export const ratingBtn = () => {
    const reqPost = { cancel: null };
    const { cancelToken, isCancel } = generateCancelRequest(reqPost);
    const postId = document.querySelector('#post-id');

    document.querySelectorAll('.rating').forEach($rating => {
        $rating.addEventListener('click', e => {
            const target = e.target.closest('.rating__btn');

            if (!target) {
                return;
            }
            const host = document.location.origin;
            const vote = target.getAttribute('data-vote');
            let data = '';

            document.querySelectorAll('.rating__empty').forEach(rating => {
                rating.classList.add('not-allowed');
            });

            if (getCookie('vote')) {
                return false;
            } else {
                data = `&type=new&vote=${vote}&id=${postId.value}&action=ajax_rating`;

                setCookie('vote', vote, { path: window.location.pathname, ['max-age']: 24 * 3600 });

                (async() => {
                    try {
                        const response = await axios.post(`${host}/wp-admin/admin-ajax.php`, data, {
                            cancelToken,
                        });

                        reqPost.cancel = null;

                        ratingRender(response.data);
                    } catch (error) {
                        if (isCancel(error)) {
                            return;
                        }

                        warn(error.message);
                    }
                })();
            }
        });
    });
};