export const initRatingPost = $element => {
    const $ratingFill = $element.querySelector('[data-rating-value]');
    $ratingFill.style.width = `${$element.clientWidth * (Number($ratingFill.getAttribute('data-rating-value')) / 5)}px`;
};
