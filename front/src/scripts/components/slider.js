export const initSlider = ($slider, $btns, myTimer) => {
    $btns.forEach($btn => {
        $btn.addEventListener('click', (elem) => {
            if (!elem.target.classList.contains('active')) {
                changeSlide(elem.target.getAttribute('data-btn-id'), $slider);
                clearInterval(myTimer);
                myTimer = setInterval(() => { autoNextSlide($slider, $btns); }, 6000);
            }
        })
    });
}

export const autoNextSlide = ($slider, $btns) => {
    const activeSlides = +$slider.querySelector('[data-btn-id].active').getAttribute('data-btn-id');

    if (activeSlides === $btns.length - 1) {
        changeSlide(0, $slider)
    } else {
        changeSlide(activeSlides + 1, $slider)
    }
}

const changeSlide = (number, $slider) => {
    const $activeBtn = $slider.querySelector('[data-btn-id].active');

    if ($activeBtn) {
        $activeBtn.classList.remove('active');
        const activeContent = $slider.querySelectorAll('[data-content-id].active');

        if (activeContent.length) {
            activeContent.forEach(content => {
                content.classList.remove('active');
            });
        }
    }

    $slider.querySelector(`[data-btn-id="${number}"]`).classList.add('active');
    const newContent = $slider.querySelectorAll(`[data-content-id="${number}"]`);

    if (newContent.length) {
        newContent.forEach(content => {
            content.classList.add('active');
        });
    }
}

// const mainSlider = document.querySelector('.main-slider');
// if (mainSlider) {
//     const slidesWrapperLeft = document.querySelector('.main-slider__left'),
//         slidesWrapperRight = document.querySelector('.main-slider__right'),
//         slidesFieldLeft = document.querySelector('.main-slider__left-inner'),
//         slidesFieldRight = document.querySelector('.main-slider__right-inner'),
//         slidesLeft = document.querySelectorAll('.main-slider__left-inner > img'),
//         slidesRight = document.querySelectorAll('.main-slider__right-inner > a'),
//         widthLeft = window.getComputedStyle(slidesWrapperLeft).width,
//         widthRight = window.getComputedStyle(slidesWrapperRight).width;

//     let slideIndex = 1,
//         offsetLeft = 0,
//         offsetRight = 0;

//     slidesFieldLeft.style.width = 100 * slidesLeft.length + '%';
//     slidesFieldLeft.style.display = 'flex';
//     slidesFieldLeft.style.transition = '0.5s all';

//     slidesFieldRight.style.width = 100 * slidesRight.length + '%';
//     slidesFieldRight.style.display = 'flex';
//     slidesFieldRight.style.transition = '0.5s all';

//     dots = mainSlider.querySelectorAll('.circle-button__item');

//     dots.forEach(dot => {
//         dot.addEventListener('click', (e) => {
//             const slideTo = e.target.getAttribute('data-btn-id'),
//                 slidesWrapperLeft = document.querySelector('.main-slider__left'),
//                 slidesWrapperRight = document.querySelector('.main-slider__right'),
//                 widthLeft = window.getComputedStyle(slidesWrapperLeft).width,
//                 widthRight = window.getComputedStyle(slidesWrapperRight).width;

//             slideIndex = slideTo;
//             offsetLeft = deleteNotDigits(widthLeft) * (slideTo - 1);
//             offsetRight = deleteNotDigits(widthRight) * (slideTo - 1);

//             slidesFieldLeft.style.transform = `translateX(-${offsetLeft}px)`;
//             slidesFieldRight.style.transform = `translateX(-${offsetRight}px)`;

//             //dotsActive(dots, slideIndex);
//         });
//     });

// }











// const slidesWrapper = document.querySelector('.header-content__slider-wrap');

// if (slidesWrapper) {
//   const slides = document.querySelectorAll('.header-content__slider-item'),
//         slidesField = document.querySelector('.header-content__slider-inner'),
//         width = window.getComputedStyle(slidesWrapper).width;

//   let slideIndex = 1,
//       offset = 0;

//   slidesField.style.width = 100 * slides.length + '%';
//   slidesField.style.display = 'flex';
//   slidesField.style.transition = '0.5s all';

//   slidesWrapper.style.overflow = 'hidden';

//   slides.forEach(slide => {
//     slide.style.width = width;
//   });

//   const indicators = document.querySelector('.header-content__slider-btn'),
//       dots = [];

//   for (let i = 0; i < slides.length; i++) {
//     const dot = document.createElement('div');
//     dot.setAttribute('data-slide-to', i+1);
//     dot.classList.add('header-content__slider-dot');
//     if (i == 0) {
//       dot.classList.add('select');
//     }
//     indicators.append(dot);
//     dots.push(dot);
//   }

//   dots.forEach(dot => {
//     dot.addEventListener('click', (e) => {
//       const slideTo = e.target.getAttribute('data-slide-to'),
//             slidesWrapper = document.querySelector('.header-content__slider-wrap'),
//             width = window.getComputedStyle(slidesWrapper).width;

//       slideIndex = slideTo;
//       offset = deleteNotDigits(width) * (slideTo - 1);

//       slidesField.style.transform = `translateX(-${offset}px)`;

//       dotsActive(dots, slideIndex);
//     });
//   });
// }


// function deleteNotDigits(str) {
//   return +str.replace(/\D/ig, '');
// }

// function dotsActive(dots, slideIndex) {
//   dots.forEach(dot => dot.classList.remove('select'));
//   dots[slideIndex - 1].classList.add('select');
// };


// function nextSlide() {
//   const slidesWrapper = document.querySelector('.header-content__slider-wrap');

//   if (slidesWrapper) {
//     const slidesField = document.querySelector('.header-content__slider-inner'),
//           dots = document.querySelectorAll('.header-content__slider-dot'),
//           width = window.getComputedStyle(slidesWrapper).width,
//           slides = document.querySelectorAll('.header-content__slider-item');

//     let slideIndex = document.querySelector('.header-content__slider-dot.select').getAttribute('data-slide-to');

//     if (slideIndex == slides.length) {
//       slideIndex = 1;
//     } else {
//       slideIndex++;
//     }

//     slidesField.style.transform = `translateX(-${deleteNotDigits(width) * (slideIndex - 1)}px)`;

//     dotsActive(dots, slideIndex);
//   }
// }


// setInterval(nextSlide, 10000);


// window.addEventListener("resize", () => {
//   const slidesWrapper = document.querySelector('.header-content__slider-wrap');

//   if (slidesWrapper) {
//     const slidesField = document.querySelector('.header-content__slider-inner'),
//           width = window.getComputedStyle(slidesWrapper).width,
//           slides = document.querySelectorAll('.header-content__slider-item'),
//           slideTo = document.querySelector('.header-content__slider-dot.select').getAttribute('data-slide-to');

//     slides.forEach(slide => {
//       slide.style.width = width;
//     });

//     slidesField.style.transform = `translateX(-${deleteNotDigits(width) * (slideTo - 1)}px)`;
//   }
// });