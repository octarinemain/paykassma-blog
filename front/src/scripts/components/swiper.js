import Splide from '@splidejs/splide';

export function initSliders() {
    if (document.getElementById('main-slider') !== null) {
        new Splide('#main-slider', {
            type: 'loop',
            focus: 'center',
            speed: 250,
            rewind: true,
            autoHeight: true,
            pagination: false,
            padding: {
                left: 60,
                right: 60,
            },
            perPage: 3,
            perMove: 1,
            start: 1,
            breakpoints: {
                1199: {
                    padding: {
                        left: 40,
                        right: 40,
                    },
                },
                991: {
                    perPage: 2,
                    padding: {
                        left: 25,
                        right: 25,
                    },
                },
                479: {
                    perPage: 1,
                    padding: {
                        left: 35,
                        right: 35,
                    },
                },
            },
        }).mount();
    }
}
