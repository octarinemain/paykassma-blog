export const wrapping = ($el, type) => {
    const $wrap = document.createElement('div');
    switch (type) {
        case 'table':
            $wrap.classList.add('table-wrap');
            break;
        case 'iframe':
            $wrap.classList.add('video-wrap');
            break;
    }
    const $newEl = $el.cloneNode(true);
    $wrap.append($newEl);
    $el.replaceWith($wrap);
};
