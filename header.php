<?php

/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package blog_paykassa
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>

<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <link rel="profile" href="https://gmpg.org/xfn/11">

    <style>
    @font-face {
        font-family: "Montserrat";
        src: url("<?php echo get_template_directory_uri(); ?>/front/dist/fonts/Montserrat/Montserrat-Bold.woff2") format("woff2"),
            url("<?php echo get_template_directory_uri(); ?>/front/dist/fonts/Montserrat/Montserrat-Bold.woff") format("woff");
        font-weight: 700;
        font-style: normal;
        font-display: swap;
    }


    @font-face {
        font-family: "Montserrat";
        src: url("<?php echo get_template_directory_uri(); ?>/front/dist/fonts/Montserrat/Montserrat-BoldItalic.woff2") format("woff2"),
            url("<?php echo get_template_directory_uri(); ?>/front/dist/fonts/Montserrat/Montserrat-BoldItalic.woff") format("woff");
        font-weight: 700;
        font-style: italic;
        font-display: swap;
    }


    @font-face {
        font-family: "Montserrat";
        src: url("<?php echo get_template_directory_uri(); ?>/front/dist/fonts/Montserrat/Montserrat-SemiBold.woff2") format("woff2"),
            url("<?php echo get_template_directory_uri(); ?>/front/dist/fonts/Montserrat/Montserrat-SemiBold.woff") format("woff");
        font-weight: 600;
        font-style: normal;
        font-display: swap;
    }

    @font-face {
        font-family: "Montserrat";
        src: url("<?php echo get_template_directory_uri(); ?>/front/dist/fonts/Montserrat/Montserrat-SemiBoldItalic.woff2") format("woff2"),
            url("<?php echo get_template_directory_uri(); ?>/front/dist/fonts/Montserrat/Montserrat-SemiBoldItalic.woff") format("woff");
        font-weight: 600;
        font-style: italic;
        font-display: swap;
    }

    @font-face {
        font-family: "Montserrat";
        src: url("<?php echo get_template_directory_uri(); ?>/front/dist/fonts/Montserrat/Montserrat-Medium.woff2") format("woff2"),
            url("<?php echo get_template_directory_uri(); ?>/front/dist/fonts/Montserrat/Montserrat-Medium.woff") format("woff");
        font-weight: 500;
        font-style: normal;
        font-display: swap;
    }

    @font-face {
        font-family: "Montserrat";
        src: url("<?php echo get_template_directory_uri(); ?>/front/dist/fonts/Montserrat/Montserrat-MediumItalic.woff2") format("woff2"),
            url("<?php echo get_template_directory_uri(); ?>/front/dist/fonts/Montserrat/Montserrat-MediumItalic.woff") format("woff");
        font-weight: 500;
        font-style: italic;
        font-display: swap;
    }

    @font-face {
        font-family: "Montserrat";
        src: url("<?php echo get_template_directory_uri(); ?>/front/dist/fonts/Montserrat/Montserrat-Regular.woff2") format("woff2"),
            url("<?php echo get_template_directory_uri(); ?>/front/dist/fonts/Montserrat/Montserrat-Regular.woff") format("woff");
        font-weight: 400;
        font-style: normal;
        font-display: swap;
    }
    </style>
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div class="wrap-content">
    <?php wp_body_open(); ?>
    <div class="single-background-img">
        <?php echo get_the_post_thumbnail(); ?>
    </div>
    <header class="header">
        <div class="container">
            <div class="header-content js-header">
                <a href="https://paykassma.com/" class="main-logo">
                    <svg>
                        <use xlink:href="<?php echo get_template_directory_uri(); ?>/front/dist/images/svg/sprite.svg#icon-logo"></use>
                    </svg>
                </a>
                <button type="button" class="hamburger hamburger--elastic js-burger-btn">
                    <span class="hamburger-box">
                        <span class="hamburger-inner"></span>
                    </span>
                </button>
                <div class="header-content__burger-menu js-burger-menu">
                    <nav class="header-content__burger-menu-item">
                        <?php
                        wp_nav_menu(
                            array(
                                'theme_location' => 'menu-1',
                                'menu_id'        => 'primary-burger-menu',
                            )
                        );
                        ?>
                        <a href="<?php echo get_option('home'); ?>">Home Page</a>
                    </nav><!-- .header-content__menu-item -->
                </div>

                <div class="menu-desktop-wrap">
                    <nav class="menu-desktop__menu">
                        <?php
                        wp_nav_menu(
                            array(
                                'theme_location' => 'menu-1',
                                'menu_id'        => 'primary-menu',
                                'walker' => new submenu_wrap()
                            )
                        );
                        ?>
                    </nav>
                    <a href="<?php echo get_option('home'); ?>" class="menu-desktop__btn-home">Home Page</a>
                </div>
            </div><!-- .header-content -->
        </div><!-- .container -->
    </header><!-- .header -->
