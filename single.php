<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package blog_paykassa
 */

get_header();
?>
<input hidden id="post-id" value="<?php echo get_the_ID(); ?>">
<span class="rating__value" style="display: none;" itemprop="ratingValue"><?php echo number_format(get_field('average', get_the_ID()), 2); ?></span>
<div class="container single-social-networks">
    <div class="social-networks">
        <?php if(get_field('instagram', 'option')){ ?>
            <noindex><a class="social-networks__item instagram" rel="nofollow" href="<?php the_field('instagram', 'option'); ?>">
                <svg viewBox="0 0 17 17" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M16.945 5.018a6.144 6.144 0 0 0-.395-2.089A4.344 4.344 0 0 0 14.064.448 6.246 6.246 0 0 0 12 .052C11.092.012 10.804 0 8.5 0 6.193 0 5.897 0 4.996.052a6.245 6.245 0 0 0-2.063.396A4.35 4.35 0 0 0 .448 2.929a6.168 6.168 0 0 0-.395 2.06C.013 5.895 0 6.182 0 8.484c0 2.301 0 2.595.053 3.496.014.705.147 1.4.395 2.062a4.344 4.344 0 0 0 2.487 2.48c.658.258 1.356.401 2.063.424.909.04 1.197.053 3.502.053 2.305 0 2.6 0 3.502-.053a6.228 6.228 0 0 0 2.064-.395 4.357 4.357 0 0 0 2.486-2.481c.248-.66.381-1.355.395-2.061.04-.907.053-1.194.053-3.497-.002-2.301-.002-2.593-.055-3.495Zm-8.452 7.82A4.36 4.36 0 0 1 4.13 8.48a4.36 4.36 0 0 1 4.364-4.357c1.158 0 2.268.46 3.086 1.276a4.353 4.353 0 0 1 0 6.162 4.368 4.368 0 0 1-3.086 1.276Zm4.538-7.86a1.018 1.018 0 0 1-1.017-1.016 1.014 1.014 0 0 1 1.017-1.015 1.019 1.019 0 0 1 1.017 1.015c0 .562-.455 1.016-1.017 1.016Z" fill="url(#a)"/><path d="M8.5 11a2.5 2.5 0 1 0 0-5 2.5 2.5 0 0 0 0 5Z" fill="url(#b)"/><defs><linearGradient id="a" x1="14.571" y1="1.821" x2="0" y2="17" gradientUnits="userSpaceOnUse"><stop stop-color="#9E45AF"/><stop offset=".502" stop-color="#E4265B"/><stop offset="1" stop-color="#FFDB0F"/></linearGradient><linearGradient id="b" x1="8.5" y1="6" x2="8.5" y2="11" gradientUnits="userSpaceOnUse"><stop stop-color="#BD3180"/><stop offset="1" stop-color="#EF6B3A"/></linearGradient></defs></svg>
            </a></noindex>
        <?php } ?>
        <?php if(get_field('telegram', 'option')){ ?>
        <noindex><a class="social-networks__item telegram" rel="nofollow" href="<?php the_field('telegram', 'option'); ?>">
            <svg>
                <use xlink:href="<?php echo get_template_directory_uri(); ?>/front/dist/images/svg/sprite.svg#icon-telegram"></use>
            </svg>
        </a></noindex>
        <?php } ?>
        <?php if(get_field('twitter', 'option')){ ?>
        <noindex><a class="social-networks__item twitter" rel="nofollow" href="<?php the_field('twitter', 'option'); ?>">
            <svg>
                <use xlink:href="<?php echo get_template_directory_uri(); ?>/front/dist/images/svg/sprite.svg#icon-twitter"></use>
            </svg>
        </a></noindex>
        <?php } ?>
        <?php if(get_field('facebook', 'option')){
        ?>
        <noindex><a class="social-networks__item facebook" rel="nofollow" href="<?php the_field('facebook', 'option'); ?>">
            <svg>
                <use xlink:href="<?php echo get_template_directory_uri(); ?>/front/dist/images/svg/sprite.svg#icon-facebook"></use>
            </svg>
        </a></noindex>
        <?php } ?>
    </div>
</div>
<main class="content container">
    <div class="single-content-wrap">
        <div class="single-content">
            <div class="single-content__info">
                <div class="single-beardcrumbs"><?php if( function_exists('kama_breadcrumbs') ) kama_breadcrumbs(' / '); ?></div>
                <div class="single-content__info-post">
                    <span class="single-bg-figure-offer"></span>
                    <span class="single-content__info-post-date"><?php echo get_the_date('F j, Y'); ?></span>

                    <div class="single-content__info-post-wrap-views-rating">
                        <div class="rating rating-top" itemprop="review" itemscope itemtype="https://schema.org/Review">
                            <div class="prelude-statistics__info-rating" itemscope itemprop="reviewRating" itemtype="http://schema.org/AggregateRating">
                                <meta itemprop="bestRating" content="5">
                                <meta itemprop="worstRating" content="1">
                                <div class="rating__empty">
                                    <div class="rating__blue"></div>
                                    <div class="rating__white">
                                        <svg class="rating__btn" data-vote="5" width="16" height="15" viewBox="0 0 12 11" fill="none"
                                            xmlns="http://www.w3.org/2000/svg">
                                            <path
                                                d="M6.28564 0L7.63273 4.1459H11.992L8.46527 6.7082L9.81236 10.8541L6.28564 8.2918L2.75893 10.8541L4.10602 6.7082L0.579306 4.1459H4.93856L6.28564 0Z"
                                                fill="#474747" />
                                        </svg>
                                        <svg class="rating__btn" data-vote="4" width="16" height="15" viewBox="0 0 12 11" fill="none"
                                            xmlns="http://www.w3.org/2000/svg">
                                            <path
                                                d="M6.28564 0L7.63273 4.1459H11.992L8.46527 6.7082L9.81236 10.8541L6.28564 8.2918L2.75893 10.8541L4.10602 6.7082L0.579306 4.1459H4.93856L6.28564 0Z"
                                                fill="#474747" />
                                        </svg>
                                        <svg class="rating__btn" data-vote="3" width="16" height="15" viewBox="0 0 12 11" fill="none"
                                            xmlns="http://www.w3.org/2000/svg">
                                            <path
                                                d="M6.28564 0L7.63273 4.1459H11.992L8.46527 6.7082L9.81236 10.8541L6.28564 8.2918L2.75893 10.8541L4.10602 6.7082L0.579306 4.1459H4.93856L6.28564 0Z"
                                                fill="#474747" />
                                        </svg>
                                        <svg class="rating__btn" data-vote="2" width="16" height="15" viewBox="0 0 12 11" fill="none"
                                            xmlns="http://www.w3.org/2000/svg">
                                            <path
                                                d="M6.28564 0L7.63273 4.1459H11.992L8.46527 6.7082L9.81236 10.8541L6.28564 8.2918L2.75893 10.8541L4.10602 6.7082L0.579306 4.1459H4.93856L6.28564 0Z"
                                                fill="#474747" />
                                        </svg>
                                        <svg class="rating__btn" data-vote="1" width="16" height="15" viewBox="0 0 12 11" fill="none"
                                            xmlns="http://www.w3.org/2000/svg">
                                            <path
                                                d="M6.28564 0L7.63273 4.1459H11.992L8.46527 6.7082L9.81236 10.8541L6.28564 8.2918L2.75893 10.8541L4.10602 6.7082L0.579306 4.1459H4.93856L6.28564 0Z"
                                                fill="#474747" />
                                        </svg>
                                    </div>
                                </div>
                            </div>
                            <span class="votes__value" itemprop="ratingCount">
                                <?php the_field('votes_count', get_the_ID()); ?>
                            </span>
                        </div>
                        <span class="single-content__info-post-views">
                                <svg>
                                    <use xlink:href="<?php echo get_template_directory_uri(); ?>/front/dist/images/svg/sprite.svg#icon-viewed"></use>
                                </svg>
                            <?php $views = pvc_get_post_views(get_the_ID());
                            echo number_format($views); ?>
                        </span>


                    </div>
                </div>
                <div class="single-content__info-company">
                    <div class="author">
                        <div class="author-img">
                            <img src="<?php the_field('author_photo', 'user_' . get_the_author_meta('ID')) ?>" alt="">
                        </div>
                        <div class="author-info">
                            <p class="author-info__name"><?php echo get_the_author(); ?></p>
                            <p class="author-info__role"><?php the_field('author_role', 'user_' . get_the_author_meta('ID')); ?></p>
                        </div>
                    </div>
                    <div class="social-networks">
                        <?php if(get_field('instagram', 'option')){ ?>
                            <noindex><a class="social-networks__item instagram" rel="nofollow" href="<?php the_field('instagram', 'option'); ?>">
                                <svg viewBox="0 0 17 17" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M16.945 5.018a6.144 6.144 0 0 0-.395-2.089A4.344 4.344 0 0 0 14.064.448 6.246 6.246 0 0 0 12 .052C11.092.012 10.804 0 8.5 0 6.193 0 5.897 0 4.996.052a6.245 6.245 0 0 0-2.063.396A4.35 4.35 0 0 0 .448 2.929a6.168 6.168 0 0 0-.395 2.06C.013 5.895 0 6.182 0 8.484c0 2.301 0 2.595.053 3.496.014.705.147 1.4.395 2.062a4.344 4.344 0 0 0 2.487 2.48c.658.258 1.356.401 2.063.424.909.04 1.197.053 3.502.053 2.305 0 2.6 0 3.502-.053a6.228 6.228 0 0 0 2.064-.395 4.357 4.357 0 0 0 2.486-2.481c.248-.66.381-1.355.395-2.061.04-.907.053-1.194.053-3.497-.002-2.301-.002-2.593-.055-3.495Zm-8.452 7.82A4.36 4.36 0 0 1 4.13 8.48a4.36 4.36 0 0 1 4.364-4.357c1.158 0 2.268.46 3.086 1.276a4.353 4.353 0 0 1 0 6.162 4.368 4.368 0 0 1-3.086 1.276Zm4.538-7.86a1.018 1.018 0 0 1-1.017-1.016 1.014 1.014 0 0 1 1.017-1.015 1.019 1.019 0 0 1 1.017 1.015c0 .562-.455 1.016-1.017 1.016Z" fill="url(#aa)"/><path d="M8.5 11a2.5 2.5 0 1 0 0-5 2.5 2.5 0 0 0 0 5Z" fill="url(#bb)"/><defs><linearGradient id="aa" x1="14.571" y1="1.821" x2="0" y2="17" gradientUnits="userSpaceOnUse"><stop stop-color="#9E45AF"/><stop offset=".502" stop-color="#E4265B"/><stop offset="1" stop-color="#FFDB0F"/></linearGradient><linearGradient id="bb" x1="8.5" y1="6" x2="8.5" y2="11" gradientUnits="userSpaceOnUse"><stop stop-color="#BD3180"/><stop offset="1" stop-color="#EF6B3A"/></linearGradient></defs></svg>
                            </a></noindex>
                        <?php } ?>
                        <?php if(get_field('telegram', 'option')){ ?>
                        <noindex><a class="social-networks__item telegram" rel="nofollow" href="<?php the_field('telegram', 'option'); ?>">
                            <svg>
                                <use xlink:href="<?php echo get_template_directory_uri(); ?>/front/dist/images/svg/sprite.svg#icon-telegram"></use>
                            </svg>
                        </a></noindex>
                        <?php } ?>
                        <?php if(get_field('twitter', 'option')){ ?>
                        <noindex><a class="social-networks__item twitter" rel="nofollow" href="<?php the_field('twitter', 'option'); ?>">
                            <svg>
                                <use xlink:href="<?php echo get_template_directory_uri(); ?>/front/dist/images/svg/sprite.svg#icon-twitter"></use>
                            </svg>
                        </a></noindex>
                        <?php } ?>
                        <?php if(get_field('facebook', 'option')){
                        ?>
                        <noindex><a class="social-networks__item facebook" rel="nofollow" href="<?php the_field('facebook', 'option'); ?>">
                            <svg>
                                <use xlink:href="<?php echo get_template_directory_uri(); ?>/front/dist/images/svg/sprite.svg#icon-facebook"></use>
                            </svg>
                        </a></noindex>
                        <?php } ?>
                    </div>
                </div>
            </div>

            <div class="visual-editor">
                <h1><?php the_title(); ?></h1>
                <?php the_content(); ?>
            </div>

            <div class="author-information">
                <div class="author">
                    <div class="author-img">
                        <img src="<?php the_field('author_photo', 'user_' . get_the_author_meta('ID')) ?>" alt="">
                    </div>
                    <div class="author-info">
                        <p class="author-info__name"><?php echo get_the_author(); ?></p>
                        <p class="author-info__role"><?php the_field('author_role', 'user_' . get_the_author_meta('ID')); ?></p>
                    </div>
                </div>
                <p class="author-information__bio"><?php the_field('author_bio', 'user_' . get_the_author_meta('ID')); ?></p>
            </div>
            <!-- <div class="categorys-wrap">
                <?php $cats = wp_get_post_terms(get_the_ID(), 'category', array('fields' => 'names'));
                        $catsId = wp_get_post_terms(get_the_ID(), 'category', array('fields' => 'ids'));
                if(!empty($cats)) { $i=0; foreach ($cats as $cat):?>
                    <a class="single-content__info-post-category" href="<?php echo get_category_link($catsId[$i]);?>"><?php echo $cat;?></a>
                <?php $i+=1; endforeach; } ?>
            </div> -->
			<?php
			$post_id_nav = $post->ID; // Get current post ID
			$cat_nav = get_the_category(); 
			$current_cat_id_nav = $cat_nav[0]->cat_ID; // Get current Category ID 

			$args_nav = array('category'=>$current_cat_id_nav,'orderby'=>'post_date','order'=> 'DESC');
			$posts_nav = get_posts($args_nav);
			// Get IDs of posts retrieved by get_posts function
			$ids_nav = array();
			foreach ($posts_nav as $thepost_nav) {
				$ids_nav[] = $thepost_nav->ID;
			}
			// Get and Echo the Previous and Next post link within same Category
			$index_nav = array_search($post_nav->ID, $ids_nav);
			$prev_post_nav = $ids_nav[$index_nav+1];
			$next_post_nav = $ids_nav[$index_nav-1];
			?>
            <div class="single-navigation">
				<?php if (!empty($prev_post_nav)){ ?> <a rel="prev" href="<?php echo get_permalink($prev_post_nav) ?>">Previous</a> <?php } ?>
                <?php /*?><?php previous_post_link('%link', 'Previous post', true); ?><?php */?>

                <div class="rating" itemprop="review" itemscope itemtype="https://schema.org/Review">
                    <div class="prelude-statistics__info-rating" itemscope itemprop="reviewRating" itemtype="http://schema.org/AggregateRating">
                        <meta itemprop="bestRating" content="5">
                        <meta itemprop="worstRating" content="1">
                        <div class="rating__empty">
                            <div class="rating__blue"></div>
                            <div class="rating__white">
                                <svg class="rating__btn" data-vote="5" width="16" height="15" viewBox="0 0 12 11" fill="none"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path
                                        d="M6.28564 0L7.63273 4.1459H11.992L8.46527 6.7082L9.81236 10.8541L6.28564 8.2918L2.75893 10.8541L4.10602 6.7082L0.579306 4.1459H4.93856L6.28564 0Z"
                                        fill="#474747" />
                                </svg>
                                <svg class="rating__btn" data-vote="4" width="16" height="15" viewBox="0 0 12 11" fill="none"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path
                                        d="M6.28564 0L7.63273 4.1459H11.992L8.46527 6.7082L9.81236 10.8541L6.28564 8.2918L2.75893 10.8541L4.10602 6.7082L0.579306 4.1459H4.93856L6.28564 0Z"
                                        fill="#474747" />
                                </svg>
                                <svg class="rating__btn" data-vote="3" width="16" height="15" viewBox="0 0 12 11" fill="none"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path
                                        d="M6.28564 0L7.63273 4.1459H11.992L8.46527 6.7082L9.81236 10.8541L6.28564 8.2918L2.75893 10.8541L4.10602 6.7082L0.579306 4.1459H4.93856L6.28564 0Z"
                                        fill="#474747" />
                                </svg>
                                <svg class="rating__btn" data-vote="2" width="16" height="15" viewBox="0 0 12 11" fill="none"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path
                                        d="M6.28564 0L7.63273 4.1459H11.992L8.46527 6.7082L9.81236 10.8541L6.28564 8.2918L2.75893 10.8541L4.10602 6.7082L0.579306 4.1459H4.93856L6.28564 0Z"
                                        fill="#474747" />
                                </svg>
                                <svg class="rating__btn" data-vote="1" width="16" height="15" viewBox="0 0 12 11" fill="none"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path
                                        d="M6.28564 0L7.63273 4.1459H11.992L8.46527 6.7082L9.81236 10.8541L6.28564 8.2918L2.75893 10.8541L4.10602 6.7082L0.579306 4.1459H4.93856L6.28564 0Z"
                                        fill="#474747" />
                                </svg>
                            </div>
                        </div>
                    </div>
                    <span class="votes__value" itemprop="ratingCount">
                        <?php the_field('votes_count', get_the_ID()); ?>
                    </span>
                </div>
                <?php /*?><?php next_post_link('%link', 'Next post', true); ?><?php */?>
				<?php if (!empty($next_post_nav)){ ?> <a rel="next" href="<?php echo get_permalink($next_post_nav) ?>">Next</a><?php } ?>
            </div>
        </div>
    </div>

    <div class="related-posts">
        <?php
        $wp_query = new WP_Query(array(
            'posts_per_page' => 4,
            'meta_query' => array(
                array(
                    'key'     => 'average',
                    'value'   => 4.5,
                    'compare' => '>',
                    'type'    => 'NUMERIC'
                ),
            ),
            'orderby' => 'rand',
        ));
        ?>
        <?php if ($wp_query->have_posts()) : ?>
           <h2 class="related-posts__title">You may like</h2>
           <div class="posts__item-wrap">
                <?php while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
                    <a href="<?php the_permalink(); ?>" class="posts__item">
                        <div class="posts__item-img">
                            <?php echo get_the_post_thumbnail(get_the_ID()); ?>
                            <div class="posts__categories">
                                <?php $cats = wp_get_post_terms(get_the_ID(), 'post_tag', array('fields' => 'names'));
                                    $j=0;
                                    foreach ($cats as $cat) :
                                        if($j<3) {
                                ?>
                                <p>
                                    <span class="posts__categories-item">
                                        <?php echo $cat; ?>
                                    </span>
                                </p>
                                <?php $j++; } endforeach; ?>
                            </div>
                        </div>
                        <p class="posts__item-title"><?php the_title(); ?></p>
                        <p class="posts__item-subtitle"><?php echo get_the_excerpt(); ?></p>
                        <p href="<?php the_permalink(); ?>" class="read-more-btn">Read more</p>
                    </a>
                <?php endwhile; ?>
                <div class="posts__item posts__item--empty"></div>
                <div class="posts__item posts__item--empty"></div>
                <div class="posts__item posts__item--empty"></div>
                <div class="posts__item posts__item--empty"></div>
            </div> 
        <?php endif; ?>
    </div> 
</main><!-- #main -->

<?php
get_footer();